#include<iostream>
#include<stdio.h>
//#include<conio.h>
#include<stdint.h>

using namespace std;

class ByteConverter
{
    public:
        //Convert 32 bits which is stored in BigEndian format to integer. This is performed with the help of bit operation i.e left shifting and operating or.
    static int32_t bigEndianIntRead(char *fileBuf, int startIndex)
    {
        return (((fileBuf[startIndex + 0] & 0xff) << 24) | ((fileBuf[ + 1] & 0xff) << 16)| ((fileBuf[startIndex + 2] & 0xff) << 8) | ((fileBuf[startIndex + 3] & 0xff)));
    }
    //Convert 32 bits which is stored in BigEndian format to integer. This is performed with the help of bit operation i.e left shifting and operating or.
    static int32_t littleEndianIntRead(char *fileBuf, int startIndex)
    {
        return (((fileBuf[startIndex + 3] & 0xff) << 24) | ((fileBuf[startIndex + 2] & 0xff) << 16) | ((fileBuf[startIndex + 1] & 0xff) << 8) | ((fileBuf[startIndex + 0] & 0xff)));
    }
    //Convert 64 bits or 8 Byte which is stored in BigEndian format to integer. This is performed with the help of bit operation i.e left shifting and operating or.
    static double littleEndianDoubleRead(char *fileBuf,int startIndex)
    {
        double convert;
        char *add;
        int j;
        add = new char();
        j=-1;
        for(int i=startIndex; i<startIndex+8; i++)
        {
            j++;
            add[j] = fileBuf[i];
        }
        convert = *reinterpret_cast<double * const>(add);
        return convert;
    }
};

//Class HeaderShapefile have all funtion implemented to desribe every field of shapefile header file.
class HeaderShapefile
{
    public:

    //This funtion of the class will calculate the bounding box coordinates of the shapefile.
    //Following values i.e x and y minimum and maximum values also with z and m minimum
    // and maximum values are obtained.


    static double dimensionXMin(char*fileBuf, int startIndex)
    {
        return ByteConverter::littleEndianDoubleRead(fileBuf,startIndex);
    }

    static double dimensionYmin(char*fileBuf, int startIndex)
    {
        return ByteConverter::littleEndianDoubleRead(fileBuf, startIndex);
    }

    static double dimensionXmax(char*fileBuf, int startIndex)
    {
        return ByteConverter::littleEndianDoubleRead(fileBuf, startIndex);
    }

    static double dimensionYmax(char*fileBuf, int startIndex)
    {
        return ByteConverter::littleEndianDoubleRead(fileBuf, startIndex);
    }
};

class SizeOfFile
{
    public:
        //This function finds the size of file in Byte
    static long sizeOfFiles(FILE *file)
    {
            long l, e;
            l = ftell(file);
            fseek(file, 0, 2);
            e = ftell(file);
            fseek(file, l, 0);
            return e;
    }
};

int main()
{
    int32_t filecodes, fileLengths, shapeTypes, versions;
    double xmin, ymin, xmax, ymax, mmin, mmax, zmin, zmax;
    string shape;
    char *filePath = "places.shp";
	char*fileBuf;			// Pointer to our buffered data
	FILE *file = NULL;		// File pointer
	// Open the file in binary mode using the "rb" format string
	// This also checks if the file exists and/or can be opened for reading correctly
	if ((file = fopen(filePath, "rb")) == NULL)
		cout << "Could not open specified file" << endl;
	else
		cout << "File opened successfully" << endl;

	// Get the size of the file in bytes
	long fileSize = SizeOfFile::sizeOfFiles(file);

	// Allocate space in the buffer for the whole file
	fileBuf = new char[fileSize];

	// Read the file in to the buffer
	fread(fileBuf, fileSize, 1, file);

	// Now that we have the entire file buffered, we can take a look at some binary infomation

	cout<<"File size = " <<fileSize;

    xmin = HeaderShapefile::dimensionXMin(fileBuf,36);
    ymin = HeaderShapefile::dimensionYmin(fileBuf,44);
    xmax = HeaderShapefile::dimensionXmax(fileBuf,52);
    ymax = HeaderShapefile::dimensionYmax(fileBuf,60);

    /*****************MINIMUM BOUNDING BOX OF SHAPEFILE DETAIL*********************/

    cout<<endl<<"\n/*****************MINIMUM BOUNDING BOX SHAPEFILE DETAIL*********************/\n\n";

    cout<<"X minimum = "<<xmin<<endl;
    cout<<"Y minimum = "<<ymin<<endl;
    cout<<"X maximum = "<<xmax<<endl;
    cout<<"Y maximum = "<<ymax<<endl;

	cin.get();
	delete[]fileBuf;
        fclose(file);   // Almost forgot this
	return 0;
}


